const path = require("path");
const Order = require(path.join(__dirname, "..", "models", "OrderModel"));

exports.getOrders = async (req, res) => {
  // Simulate a delay with the API response
  try {
    const data = await Order.fetchAll();
    res.status(200).send(data);
  } catch (error) {
    // Maybe do some sort of logging here
    res.status("500").send({ message: `Error: ${error}` });
  }
};

exports.addOrder = async (req, res) => {
  const order = new Order(Order.generateNewOrder());

  try {
    const saveOrder = await order.save();
    res.status(200).send({ message: "success" });
  } catch (error) {
    res.status("500").send({ message: `Error: ${error}` });
  }
};

exports.removeOrder = async (req, res) => {
  try {
    Order.pop();
    res.status(200).send({ message: "success" });
  } catch (error) {
    res.status("500").send({ message: `Error: ${error}` });
  }
};
