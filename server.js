require("dotenv").config();

const express = require("express");
const bodyParser = require("body-parser");
const orderRoutes = require("./routes/orderRoute");

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use("/orders", orderRoutes);

app.listen(process.env.SERVER_PORT || 8000);
