const express = require("express");
const routes = express.Router();
const path = require("path");
const orderController = require(path.join(
  __dirname,
  "..",
  "controllers",
  "orderController"
));

routes.get("/list", orderController.getOrders);
routes.get("/add", orderController.addOrder);
routes.get("/remove", orderController.removeOrder);

module.exports = routes;
