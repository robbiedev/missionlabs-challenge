/**
 * Generates a number between 0 and maximum number given.
 * @param {number} max The maxium number the function can generate.
 * @returns {number} Values ranging from 0 to the max number.
 */
generateRandBetween = (max) => {
  return Math.floor(Math.random() * max);
};

module.exports = generateRandBetween;
