import React from "react";
import PropTypes from "prop-types";

import styles from "./style.module.scss";
/**
 * An outline button component.
 * @param {string} text The text content of the button.
 * @param {function} handleOnClick The function fired when the button is clicked.
 */
const OutlineButton = ({ text, handleOnClick }) => (
  <span onClick={handleOnClick} className={styles.button}>
    {text}
  </span>
);

OutlineButton.defaultProps = {
  text: "Save",
};
OutlineButton.propTypes = {
  text: PropTypes.string,
  handleOnClick: PropTypes.func.isRequired,
};

export default OutlineButton;
