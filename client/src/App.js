import React from "react";
import { Switch, Route } from "react-router-dom";
import Orders from "./views/Orders";
import "./style.module.scss";

export default () => (
  <Switch>
    <Route exact path="/" component={Orders} />
  </Switch>
);
