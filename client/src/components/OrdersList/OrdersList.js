import React from "react";
import PropTypes from "prop-types";
import ReactPaginate from "react-paginate";
import axios from "axios";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import ReactTooltip from "react-tooltip";

import OutlineButton from "../../lib/buttons/OutlineButton";

import Statusbar from "./components/StatusBar";
import Template from "./components/Template";

import styles from "./style.module.scss";

/**
 * @export
 * @class OrdersList
 * @extends {React.Component}
 */
export default class OrdersList extends React.Component {
  state = {
    data: [],
    pageData: [],
    filters: [],
    offset: 0,
    selected: 0,
    pageCount: 1,
    serverDataLoaded: false,
    rotationInterval: null,
  };

  componentDidMount() {
    this.loadItemsFromServer();
  }

  /**
   * Load in data from the server.
   */
  loadItemsFromServer() {
    const { enableRotation } = this.props;

    setTimeout(async () => {
      try {
        const fetchData = await axios.get("/orders/list");
        const { data } = fetchData;
        this.setState({ data, serverDataLoaded: true }, () => {
          this.update();
          enableRotation && this.invokeRotation();
        });
      } catch (error) {
        // TODO: Maybe implement some sort of logging and alert the user of the problem
        console.log(error);
      }
    }, 2 * 1000);
  }

  /**
   * Auto rotates the pagination view.
   */
  invokeRotation() {
    if (this.rotationInterval !== null) {
      clearInterval(this.rotationInterval);
    }
    this.rotationInterval = setInterval(() => {
      const { selected: prevSelected, pageCount } = this.state;
      const { perPage } = this.props;
      // If we are at the end of the rotation set the new page to 0
      const selected = prevSelected + 1 < pageCount ? prevSelected + 1 : 0;
      this.setState(
        {
          offset: selected * perPage,
          selected,
        },
        () => {
          this.update();
        }
      );
    }, 10 * 1000);
  }

  /**
   * Updates the pagination list when a user selects a new view.
   * @param {number} selected The newly selected view.
   */
  handleListChange = ({ selected }) => {
    const { perPage } = this.props;
    this.setState({ selected, offset: selected * perPage }, () => {
      this.update();
    });
  };

  /**
   * Updates the filters array based on user interaction.
   * @param {string} statusFilter The filter name.
   */
  handleStatusFilterChange = (statusFilter) => {
    const { filters: prevFilters } = this.state;
    const filters = [...prevFilters];
    // Check if filter is already set, if it is toggle it off and vice versa
    const isFilterSet = filters.findIndex((filter) => filter === statusFilter);

    if (isFilterSet !== -1) {
      filters.splice(isFilterSet, 1);
    } else {
      filters.push(statusFilter);
    }
    this.setState({ filters, offset: 0, selected: 0 }, () => {
      this.update();
    });
  };

  /**
   *  Applies filters to supplied data.
   * @param {object} data The pagination data.
   * @returns {object} The newly mutated data.
   */
  applyFilters(data) {
    const { filters } = this.state;
    if (filters.length > 0) {
      data = data.filter((item) => {
        return filters.find((filter) => filter === item.status);
      });
    }
    return data;
  }

  /**
   * Determines what data the curent view should show.
   */
  update() {
    const { data: prevData, offset } = this.state;
    const { perPage } = this.props;
    const data = this.applyFilters(prevData);
    const pageData = data.slice(offset, offset + perPage);

    this.setState({
      pageData,
      pageCount: Math.ceil(data.length / perPage),
    });
  }

  /**
   * Gets the loading skeleton jsx.
   */
  getLoadingSkeleton() {
    const { perPage } = this.props;
    return (
      <SkeletonTheme color="#202020" highlightColor="#262626">
        {Array.apply(null, { length: perPage }).map((e, index) => (
          <div key={index} className={styles.skeletons}>
            <Skeleton height={90} />
          </div>
        ))}
      </SkeletonTheme>
    );
  }

  // TODO: Maybe implement some sort of debounce/throttle library
  // TODO: Flesh out GUI for these methods ie adding field names

  /**
   * Adds an item to the back-end data file.
   */
  async addItem() {
    try {
      await axios.get("/orders/add");
      this.setState({ serverDataLoaded: false, selected: 0, offset: 0 }, () => {
        this.loadItemsFromServer();
      });
    } catch (error) {
      console.log(error);
    }
  }

  /**
   * Removes an item to the back-end data file.
   */
  async removeItem() {
    try {
      await axios.get("/orders/remove");
      this.setState({ serverDataLoaded: false, selected: 0, offset: 0 }, () => {
        this.loadItemsFromServer();
      });
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    const {
      pageCount,
      selected,
      pageData,
      filters,
      serverDataLoaded,
    } = this.state;

    const getOrdersList = () =>
      serverDataLoaded
        ? pageData.map((item, index) => <Template key={index} data={item} />)
        : this.getLoadingSkeleton();

    const getPagination = (
      <ReactPaginate
        containerClassName={styles.pagination}
        previousClassName={styles.hidden}
        nextClassName={styles.hidden}
        activeLinkClassName={styles.active}
        subContainerClassName={"pages pagination"}
        activeClassName={"active"}
        forcePage={selected}
        pageCount={pageCount}
        marginPagesDisplayed={2}
        pageRangeDisplayed={20}
        onPageChange={this.handleListChange}
      />
    );

    const getPageNumberIndicator = () => {
      const currentPage = selected + 1 < 10 ? `0${selected + 1}` : selected + 1;
      const totalPages = pageCount < 10 ? `0${pageCount}` : pageCount;
      return (
        <React.Fragment>
          <strong>{currentPage}</strong>
          <span className={styles.divider}></span> {totalPages}
        </React.Fragment>
      );
    };

    return (
      <React.Fragment>
        <ReactTooltip place="top" effect="solid" type={"info"} />
        <span data-tip="Appends a new item to the end of the list.">
          <OutlineButton
            text={"Add Item"}
            handleOnClick={() => {
              this.addItem();
            }}
          />
        </span>
        <span data-tip="Removes the last item from the list.">
          <OutlineButton
            text={"Remove Item"}
            handleOnClick={() => {
              this.removeItem();
            }}
          />
        </span>

        <Statusbar
          filters={filters}
          handleStatusFilterChange={this.handleStatusFilterChange}
        />

        {getOrdersList()}
        {serverDataLoaded && (
          <div className={styles.ordersFooter}>
            <div></div>
            <div>{getPagination}</div>
            <div className={styles.pageNumberIndicator}>
              {getPageNumberIndicator()}
            </div>
          </div>
        )}
      </React.Fragment>
    );
  }
}

OrdersList.defaultProps = {
  perPage: 5,
  enableRotation: true,
};

OrdersList.propTypes = {
  perPage: PropTypes.number,
  enableRotation: PropTypes.bool,
};
