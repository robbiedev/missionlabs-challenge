import React from "react";
import PropTypes from "prop-types";

import styles from "./style.module.scss";

/**
 * Maps back-end status name to a class name.
 * @param {string} name Name of the status.
 * @returns {string} mapped status value.
 */
const mapStatusClassNames = (name) => {
  const statusList = {
    READY_TO_GO: "readyToGo",
    ON_THE_WAY: "onTheWay",
    IN_THE_QUEUE: "inTheQueue",
    OUT_OF_STOCK: "outOfStock",
  };
  return statusList[name];
};

/**
 * An individual order item.
 * @param {object} data The order item object data.
 */
const Template = ({ data }) => {
  const {
    id,
    productName,
    category,
    size,
    color,
    status,
    customerInitials,
    imageName,
  } = data;

  return (
    <div data-order-id={id} className={styles.orderItem}>
      <span
        className={[
          styles.orderStatusIndicator,
          styles[mapStatusClassNames(status)],
        ].join(" ")}
      ></span>
      <div className={styles.orderDetails}>
        <div className={styles.productIntroColumn}>
          <div
            style={{
              backgroundImage:
                "url(" +
                require(`../../../../assets/images/${imageName}`) +
                ")",
            }}
            className={styles.productImage}
          ></div>
          <div className={styles.productName}>{productName}</div>
        </div>
        <div className={styles.productCategoryColumn}>
          Category: <br />
          {category}
        </div>
        <div className={styles.productSizeColumn}>
          Size: <br />
          {size}
        </div>
        <div className={styles.productColorColumn}>
          Colour: <br />
          {color}
        </div>
        <div className={styles.productInitialsColumn}>
          <div className={styles.productInitialsContainer}>
            {customerInitials}
          </div>
        </div>
      </div>
    </div>
  );
};

Template.propTypes = {
  data: PropTypes.object.isRequired,
};

export default Template;
