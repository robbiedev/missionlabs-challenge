import React from "react";
import PropTypes from "prop-types";

import styles from "./style.module.scss";

/**
 * Generate the status bar JSX.
 * @param {array}  filters The currently filtered items.
 * @param {function}  handleStatusFilterChange The function fired when a filter item is clicked.
 */
const StatusBar = ({ filters, handleStatusFilterChange }) => (
  <div className={styles.statusWrapper}>
    <div
      className={[
        styles.statusItem,
        filters.find((item) => item === "READY_TO_GO") && styles.active,
      ].join(" ")}
      onClick={() => {
        handleStatusFilterChange("READY_TO_GO");
      }}
    >
      <span
        className={[styles.statusIndicator, styles.readyToGo].join(" ")}
      ></span>
      Ready to try
    </div>
    <div
      className={[
        styles.statusItem,
        filters.find((item) => item === "ON_THE_WAY") && styles.active,
      ].join(" ")}
      onClick={() => {
        handleStatusFilterChange("ON_THE_WAY");
      }}
    >
      <span
        className={[styles.statusIndicator, styles.onTheWay].join(" ")}
      ></span>
      On the way
    </div>
    <div
      className={[
        styles.statusItem,
        filters.find((item) => item === "IN_THE_QUEUE") && styles.active,
      ].join(" ")}
      onClick={() => {
        handleStatusFilterChange("IN_THE_QUEUE");
      }}
    >
      <span
        className={[styles.statusIndicator, styles.inTheQueue].join(" ")}
      ></span>
      In the queue
    </div>
    <div
      className={[
        styles.statusItem,
        filters.find((item) => item === "OUT_OF_STOCK") && styles.active,
      ].join(" ")}
      onClick={() => {
        handleStatusFilterChange("OUT_OF_STOCK");
      }}
    >
      <span
        className={[styles.statusIndicator, styles.outOfStock].join(" ")}
      ></span>
      Out of stock
    </div>
  </div>
);

StatusBar.propTypes = {
  filters: PropTypes.string.isRequired,
  handleStatusFilterChange: PropTypes.func.isRequired,
};

export default StatusBar;
