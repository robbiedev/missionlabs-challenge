import React from "react";
import OrdersList from "../../components/OrdersList";
import styles from "./style.module.scss";

export default () => (
  <div className={styles.wrapper}>
    <OrdersList perPage={4} />
  </div>
);
