## Available Scripts

In the project directory, you can run:

### `npm start`

Runs both node and react dev environments concurrently.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Installation

npm install or yarn will have to be run both in the project root for node and client folder for react.

## Features

- Front-end Single Page Application (SPA)
- Uses a Theme file to make switching styles easy
- Able to filter using multiple statuses
- Pagination views auto rotate after 10 seconds
- Uses css modules to hash classnames at transpile and prevent clashes
- Backend follows MVC design pattern as closely as possible - views are handled in the front-end
- Started building a component library (lib) to help keeep consistency across projects
- Ability to add new items and remove existing ones
- Front-end Uses ES6 syntax

## Potential Improvements

- If the project got bigger consider using Redux
- Resetting the auto rotate timeout after selecing a new view
- Make it fully mobile responsive
- Flesh out GUI
- Connect to a DB
- Flesh out adding and removing items ie passs an ID from the front-end of the item you want to remove.
